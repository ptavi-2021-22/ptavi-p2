#!/usr/bin/python3
# -*- coding: utf-8 -*-

import csv


class Calc():
    def __init__(self, count=0):
        self.count = count

    def add(self, operand1, operand2):
        self.count = self.count + 1
        return operand1 + operand2

    def sub(self, operand1, operand2):
        self.count = self.count + 1
        return operand1 - operand2

    def div(self, operand1, operand2):
        self.count = self.count + 1
        try:
            return operand1 / operand2
        except ZeroDivisionError:
            print("Division by zero is not allowed")

    def mul(self, operand1, operand2):
        self.count = self.count + 1
        return operand1 * operand2

    def operation(self, operand, operand1, operand2):
        if operand == "+":
            return self.add(operand1, operand2)
        elif operand == "-":
            return self.sub(operand1, operand2)
        elif operand == "/":
            return self.div(operand1, operand2)
        elif operand == "x":
            return self.mul(operand1, operand2)
        else:
            print("Bad format")


if __name__ == "__main__":
    calc1 = Calc()
    with open('operations.csv') as csvFile:
        file = csv.reader(csvFile)
        for lista in file:
            try:
                operator, op1, op2, rest_num = lista[0], lista[1], lista[2], lista[3:]
            except IndexError:
                print("Bad format")
            resultado = calc1.operation(operator, float(op1), float(op2))
            for numero in rest_num:
                try:
                    resultado = calc1.operation(operator, resultado, int(numero))
                except ValueError:
                    print("Bad format")

            print(resultado)

