#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calc():

    def __init__(self, op1, op2):
        try:
            self.operand1 = float(op1)
            self.operand2 = float(op2)
        except ValueError:
            print("Error: first and third arguments should be numbers")

    def add(self):
        return self.operand1 + self.operand2

    def sub(self):
        return self.operand1 - self.operand2


if __name__ == "__main__":
    calc1 = Calc(sys.argv[1], sys.argv[3])

    if sys.argv[2] == "+":
        result = calc1.add()
    elif sys.argv[2] == "-":
        result = calc1.sub()
    else:
        sys.exit('Operand should be + or -')

    print(result)
