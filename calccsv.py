#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Calc():
    def __init__(self, count=0):
        self.count = count

    def add(self, operand1, operand2):
        self.count = self.count + 1
        return operand1 + operand2

    def sub(self, operand1, operand2):
        self.count = self.count + 1
        return operand1 - operand2

    def div(self, operand1, operand2):
        self.count = self.count + 1
        try:
            return operand1 / operand2
        except ZeroDivisionError:
            print("Bad format")

    def mul(self, operand1, operand2):
        self.count = self.count + 1
        return operand1 * operand2

    def operation(self, operand, operand1, operand2):
        if operand == "+":
            return self.add(operand1, operand2)
        elif operand == "-":
            return self.sub(operand1, operand2)
        elif operand == "/":
            return self.div(operand1, operand2)
        elif operand == "x":
            return self.mul(operand1, operand2)
        else:
            print("Bad format")


if __name__ == "__main__":
    calc1 = Calc()

    def process_csv(fichero):
        with open(fichero) as file:
            lineas = file.readlines()
            for linea in lineas:
                linea = linea.rstrip('\n')
                lista = linea.split(',')
                try:
                    operator, op1, op2, rest_num = lista[0], lista[1], lista[2], lista[3:]
                    resultado = calc1.operation(operator, float(op1), float(op2))
                except IndexError:
                    print("Bad format")
                for numero in rest_num:
                    try:
                        resultado = calc1.operation(operator, resultado, float(numero))
                    except ValueError:
                        print("Bad format")
                print(resultado)

    process_csv('fichero.txt')


