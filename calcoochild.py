#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalcChild(calcoo.Calc):

    def div(self):
        try:
            return self.operand1 / self.operand2
        except ZeroDivisionError:
            print("Division by zero is not allowed")
        raise ValueError("Division by zero is not allowed")

    def mul(self):
        return self.operand1 * self.operand2


if __name__ == "__main__":
    calc1 = CalcChild(sys.argv[1], sys.argv[3])

    if sys.argv[2] == "+":
        result = calc1.add()
    elif sys.argv[2] == "-":
        result = calc1.sub()
    elif sys.argv[2] == "/":
        result = calc1.div()
    elif sys.argv[2] == "x":
        result = calc1.mul()
    else:
        sys.exit('Operand should be +, x, / or -')

    print(result)
